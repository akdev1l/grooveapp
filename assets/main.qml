import bb.cascades 1.3

TabbedPane{
    showTabsOnActionBar: true
    Tab{
        title: "Library"
        Page{
            attachedObjects:[
                LoginPage{
                    objectName: "login_page"
                    id: login_page
                }

            ]
            onCreationCompleted:{
                login_page.open();
            }

            titleBar: TitleBar {
                kind: TitleBarKind.FreeForm
                kindProperties: FreeFormTitleBarKindProperties {
                    CustomTitleBar {
                        title: "Library"
                        isPlaying: false
                    }
                }

            }
            content: Container{
                ListView{
                    onTriggered: {
                        media_player.setMetadata(dataModel.data(indexPath));
                        groove.getStreamKey(dataModel.data(indexPath).SongID);
                    }
                    objectName: "library_list"
                    listItemComponents: [
                        ListItemComponent{
                            type: "item"
                            SongItem {
                                
                            }
                        },
                        ListItemComponent {
                            type: "header"
                            HeaderItem {
                                
                            }
                        }

                    ]
                }
            }
        }
    }
    Tab{
        title: "Popular"
        content: PopularPage {
            
        }
    }
    Tab {
        title: "Favorites"
        content: FavoritesPage {
            
        }
    }
}

