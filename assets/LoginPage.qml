import bb.cascades 1.3

Sheet{
    content: Page{
        titleBar: TitleBar{
            kind: TitleBarKind.FreeForm
            kindProperties: FreeFormTitleBarKindProperties {
                CustomTitleBar {
                    title: "Login"
                    isPlaying: false
                }
            }
        }
        content: Container{
            Container{
                leftPadding: 5.0
                rightPadding: 5.0
                Label{
                    text: "Username:"
                }
                TextField{
                    id: username
                    hintText: "Username..."
                    inputMode: TextFieldInputMode.EmailAddress
                    input.masking: TextInputMasking.Default
                    input.submitKeyFocusBehavior: SubmitKeyFocusBehavior.Next
                    input.submitKey: SubmitKey.Next
                    text: "ealejandro"
                }
                Label{
                    text: "Password:"
                }
                TextField{
                    id: password
                    text: "rd22xd06*"
                    hintText: "Password..."
                    inputMode: TextFieldInputMode.Password
                    input.submitKey: SubmitKey.Submit
                    input.submitKeyFocusBehavior: SubmitKeyFocusBehavior.Default
                    input{
                        onSubmitted: {
                            groove.authenticate(username.text, text);
                        }
                    }
                }
                Button{
                    text: "Login"
                    onClicked:{
                        groove.authenticate(username.text, password.text);
                    }
                }
            }
        }
    }
}
