import bb.cascades 1.3
import lib.webimageview 1.0

Container{
    Container{
        Container{
            layout: DockLayout {
                
            }
            leftPadding: ui.sdu(2.0)
            rightPadding: ui.sdu(2.0)
            //background: Color.Blue
            minHeight: ui.sdu(18.0)
            Container{
                //background: Color.DarkMagenta
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                verticalAlignment: VerticalAlignment.Center
                Container {
                    //background: Color.Green
                    layout: DockLayout {
                        
                    }
                    preferredHeight: ui.sdu(18.0)
                    rightMargin: ui.sdu(2.0)
                    WebImageView {
                        imageUrl: "http://images.gs-cdn.net/static/albums/200_"+ListItemData.CoverArtFilename
                        verticalAlignment: VerticalAlignment.Center
                    }
                }
                Container {
                    verticalAlignment: VerticalAlignment.Center
                    Container {
                        //background: Color.DarkGreen
                        Label {
                            text: ListItemData.SongName
                            textStyle.fontWeight: FontWeight.Bold
                        }
                    }
                    Container {
                        //background: Color.DarkYellow
                        Label {
                            text: "By " + ListItemData.ArtistName
                        }
                    }
                    Container{
                        //background: Color.DarkRed
                        Label {
                            text: "From " + ListItemData.AlbumName
                            textStyle.color: Color.LightGray
                            textStyle.fontSize: FontSize.Default
                            textStyle.fontStyle: FontStyle.Italic
                        }
                    }
                }
            }
        }
        Container {
            minHeight: ui.sdu(1/10)
            minWidth: 720.0
            preferredWidth: 768.0
            background: Color.LightGray
        }
    }
}
