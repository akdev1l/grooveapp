import bb.cascades 1.3

Container {
    layout: StackLayout {
        orientation: LayoutOrientation.TopToBottom
    
    }
    minWidth: 720.0
    preferredWidth: 768.0
    //leftPadding: ui.sdu(2.0)
    //rightPadding: ui.sdu(2.0)
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        
        }
        Container {
            minHeight: ui.sdu(5)
            preferredHeight: ui.sdu(5)
            maxHeight: ui.sdu(5)
            minWidth: ui.sdu(2.0/10.0)
            preferredWidth: 5.0
            maxWidth: 5.0
            background: Color.create("#F86F05")
        }
        Label {
            text: ListItemData
            textStyle.fontSizeValue: 200
        }
    }
    Container {
        minHeight: 2.0
        preferredHeight: ui.sdu(1/10)
        minWidth: 720.0
        preferredWidth: 768.0
        background: Color.LightGray
            //Color.create("#F86F05")
    }
}
