import bb.cascades 1.3

Page {
    titleBar: TitleBar {
        kind: TitleBarKind.FreeForm
        kindProperties: FreeFormTitleBarKindProperties {
            CustomTitleBar {
                title: "Popular"
                isPlaying: false
            }
        }
    }
    Container {
        ListView {
            bufferedScrollingEnabled: true
            objectName: "popular_list"
            onTriggered: {
                media_player.setMetadata(dataModel.data(indexPath));
                groove.getStreamKey(dataModel.data(indexPath).SongID);
            }
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    SongItem {
                        
                    }
                },
                ListItemComponent {
                    type: "header"
                    HeaderItem {
                        
                    }
                }
            ]
        }
    }
}
