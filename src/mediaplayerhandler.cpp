#include "mediaplayerhandler.h"

MediaPlayerHandler::MediaPlayerHandler(QObject *parent) :
    QObject(parent)
{
    m_Connection = new bb::multimedia::NowPlayingConnection("GrooveApp", this);
    m_Player = new bb::multimedia::MediaPlayer(this);
    m_Controller = new bb::multimedia::NowPlayingController(this);
    m_Connection->setOverlayStyle(bb::multimedia::OverlayStyle::Fancy);
    QObject::connect(m_Connection, SIGNAL(play()), m_Player, SLOT(play()));
    QObject::connect(m_Connection, SIGNAL(play()), m_Player, SLOT(acquire()));
    QObject::connect(m_Connection, SIGNAL(pause()), m_Player, SLOT(pause()));
    QObject::connect(m_Player, SIGNAL(mediaStateChanged(bb::multimedia::MediaState::Type)), m_Connection, SLOT(setMediaState(bb::multimedia::MediaState::Type)));
    QObject::connect(m_Player, SIGNAL(durationChanged(uint)), m_Connection, SLOT(setDuration(uint)));
    QObject::connect(m_Player, SIGNAL(positionChanged(uint)), m_Connection, SLOT(setPosition(uint)));
    QObject::connect(m_Player, SIGNAL(playbackCompleted()), m_Connection, SLOT(revoke()));
}
void MediaPlayerHandler::setSourceURL(QString p_Url){
    m_Player->setSourceUrl(QUrl(p_Url));
    m_Player->play();
}
void MediaPlayerHandler::setMetadata(QVariant p_SongData){
    QVariantMap metaData;
    QVariantMap data = p_SongData.toMap();
    QString coverFilename = (data["CoverArtFilename"].toString().isEmpty())?"album.png":data["CoverArtFilename"].toString();
    metaData[bb::multimedia::MetaData::Album] = data["AlbumName"];
    metaData[bb::multimedia::MetaData::Artist] = data["ArtistName"];
    metaData[bb::multimedia::MetaData::Title] = data["SongName"];
    metaData[bb::multimedia::MetaData::ArtworkUri] = QVariant(QDir::currentPath()+"/data/images/200_"+coverFilename);
    m_Connection->setIconUrl(QUrl(metaData[bb::multimedia::MetaData::ArtworkUri].toString()));
    m_Connection->setMetaData(metaData);
}
