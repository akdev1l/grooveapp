#include "webimageview.h"
#include <bb/cascades/DockLayout>

WebImageView::WebImageView()
{
    m_Cache_Dir = new QDir(QDir::currentPath()+"/data");
    m_Cache_Dir->mkdir("images");
    m_Cache_Dir->setPath(m_Cache_Dir->absolutePath()+"/images");
    m_Image_File = new QFile(this);
    m_ImageGetter = new QNetworkAccessManager(this);
    m_Image = bb::cascades::ImageView::create().preferredHeight(100.0f).preferredWidth(100.0f);
    m_Container = bb::cascades::Container::create().preferredHeight(100.f).preferredWidth(100.0f);
    m_Container->setMinWidth(100.0f);
    m_Loading = bb::cascades::ActivityIndicator::create().preferredHeight(100.0f).preferredWidth(100.0f);
    m_Container->setLayout(bb::cascades::DockLayout::create());
    m_Container->add(m_Image);
    m_Container->add(m_Loading);
    setRoot(m_Container);
}
void WebImageView::setImageUrl(QString p_NewUrl){
    if(m_Url.toString() != p_NewUrl){
        m_Url = QUrl(p_NewUrl);
        if(m_Url.toString().right(9) == "undefined"){
            m_Url = QUrl("http://images.gs-cdn.net/static/albums/200_album.png");
        }
        m_Image_File->setFileName(m_Cache_Dir->absolutePath()+"/"+m_Url.toString().replace("http://images.gs-cdn.net/static/albums/", ""));
        qDebug() << m_Image_File->fileName();
        if(m_Image_File->exists()){
            m_Loading->setVisible(false);
            m_Loading->setRunning(false);
            m_Image_File->open(QIODevice::ReadOnly);
            m_Image->setImage(bb::cascades::Image(m_Image_File->readAll()));
            m_Image_File->close();
        }
        else{
            m_Loading->setVisible(true);
            m_Loading->setRunning(true);
            m_Image->setVisible(false);
            QNetworkRequest request(m_Url);
            request.setRawHeader("User-Agent", "GrooveApp Client 1.0");
            QObject::connect(m_ImageGetter->get(request), SIGNAL(finished()), this, SLOT(onImageReceived()));
        }
        emit imageUrlChanged();
    }
}
void WebImageView::onImageReceived(){
    QNetworkReply* image_reply = qobject_cast<QNetworkReply*>(sender());
    if(image_reply->error() == QNetworkReply::NoError){
        QByteArray image_data = image_reply->readAll();
        m_Image_File->open(QIODevice::WriteOnly);
        m_Image_File->write(image_data);
        m_Image_File->close();
        m_Image->setImage(bb::cascades::Image(image_data));
        m_Loading->setVisible(false);
        m_Loading->setRunning(false);
        m_Image->setVisible(true);
    }
    else{
        qDebug() << image_reply->errorString();
    }
}
QString WebImageView::imageUrl(){
    return m_Url.toString();
}
WebImageView::~WebImageView(){
    delete m_Image;
    delete m_Loading;
    delete m_Container;
    delete m_ImageGetter;
}
