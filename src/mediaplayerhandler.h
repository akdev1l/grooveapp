#ifndef MEDIAPLAYERHANDLER_H
#define MEDIAPLAYERHANDLER_H
#include <bb/multimedia/NowPlayingConnection>
#include <bb/multimedia/NowPlayingController>
#include <bb/multimedia/MediaPlayer>
#include <bb/multimedia/MetaData>

#include <QObject>

class MediaPlayerHandler : public QObject
{
    Q_OBJECT
public:
    explicit MediaPlayerHandler(QObject *parent = 0);
    Q_INVOKABLE void setMetadata(QVariant p_SongData);
private:
    bb::multimedia::NowPlayingConnection* m_Connection;
    bb::multimedia::MediaPlayer* m_Player;
    bb::multimedia::NowPlayingController* m_Controller;
signals:

public slots:
    void setSourceURL(QString p_Url);

};

#endif // MEDIAPLAYERHANDLER_H
