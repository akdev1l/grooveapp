#ifndef WEBIMAGEVIEW_H
#define WEBIMAGEVIEW_H
#include <bb/cascades/CustomControl>
#include <bb/cascades/ImageView>
#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/Container>

class WebImageView : public bb::cascades::CustomControl
{
    Q_OBJECT
    Q_PROPERTY(QString imageUrl READ imageUrl WRITE setImageUrl NOTIFY imageUrlChanged)
public:
    WebImageView();
    ~WebImageView();
    void setImageUrl(QString p_NewUrl);
    QString imageUrl();
private:
    QNetworkAccessManager* m_ImageGetter;
    bb::cascades::ImageView* m_Image;
    bb::cascades::ActivityIndicator* m_Loading;
    bb::cascades::Container* m_Container;
    QUrl m_Url;
    QDir* m_Cache_Dir;
    QFile* m_Image_File;
signals:
    void imageUrlChanged();
public slots:
    void onImageReceived();
};

#endif // WEBIMAGEVIEW_H
