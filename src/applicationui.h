#ifndef APPLICATIONUI_H
#define APPLICATIONUI_H

#include <QObject>
#include <bb/cascades/Sheet>
#include <bb/cascades/ListView>
#include <bb/cascades/GroupDataModel>
#include "groovehandler.h"
#include "mediaplayerhandler.h"

namespace bb {
namespace cascades {
class Application;
}
}

class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI() {}
private:
    bb::cascades::Sheet* m_LoginPage;
    bb::cascades::ListView* m_LibraryList;
    bb::cascades::ListView* m_PopularList;
    bb::cascades::ListView* m_FavoritesList;
    GrooveHandler* m_Groove;
    MediaPlayerHandler* m_Media;
private slots:
    void debugDataModel(bb::cascades::DataModel *p_DataModel);
};

#endif /* APPLICATIONUI_H */

