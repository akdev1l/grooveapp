#include "applicationui.h"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/ListView>
#include <bb/cascades/GroupDataModel>

#include "webimageview.h"


using namespace bb::cascades;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
    QObject(app)
{
    // By default the QmlDocument object is owned by the Application instance
    // and will have the lifespan of the application
    qmlRegisterType<WebImageView>("lib.webimageview", 1, 0, "WebImageView");
    QmlDocument *qml = QmlDocument::create("asset:///main.qml");

    // Create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();
    m_LoginPage = root->findChild<bb::cascades::Sheet*>("login_page");
    m_LibraryList = root->findChild<bb::cascades::ListView*>("library_list");
    m_PopularList = root->findChild<bb::cascades::ListView*>("popular_list");
    m_FavoritesList = root->findChild<bb::cascades::ListView*>("favorites_list");
    m_Groove = new GrooveHandler(this);
    m_Media = new MediaPlayerHandler(this);
    qml->setContextProperty("groove", m_Groove);
    qml->setContextProperty("media_player", m_Media);
    QObject::connect(m_Groove, SIGNAL(LoggedIn()), m_LoginPage, SLOT(close()));
    QObject::connect(m_Groove, SIGNAL(LibraryRefreshed(bb::cascades::DataModel*)), m_LibraryList, SLOT(setDataModel(bb::cascades::DataModel*)));
    QObject::connect(m_Groove, SIGNAL(PopularRefreshed(bb::cascades::DataModel*)), m_PopularList, SLOT(setDataModel(bb::cascades::DataModel*)));
    QObject::connect(m_Groove, SIGNAL(FavoritesRefreshed(bb::cascades::DataModel*)), m_FavoritesList, SLOT(setDataModel(bb::cascades::DataModel*)));
    QObject::connect(m_Groove, SIGNAL(newStreamKey(QString)), m_Media, SLOT(setSourceURL(QString)));
    // Set created root object as the application scene
    app->setScene(root);
}
void ApplicationUI::debugDataModel(bb::cascades::DataModel *p_DataModel){
    qDebug() << "FIRED!\n";
    qDebug() << p_DataModel;
}
