#include "groovehandler.h"
#include <bb/data/JsonDataAccess>
#include <QVariantList>
#include <QtNetwork/QNetworkRequest>
#include <QCryptographicHash>
#include <QDebug>
#include <bb/system/SystemToast>
#include <bb/cascades/GroupDataModel>

GrooveHandler::GrooveHandler(QObject *parent) :
    QObject(parent),
    m_BaseUrl("https://api.grooveshark.com/ws/3.0/"),
    m_ApiSecret("8fa12c2eb0f1636986f0c96b096b499a"),
    m_ApiKey("bb10_rpfapps")
{
    m_Network = new QNetworkAccessManager(this);
    m_Header.insert("wsKey", QVariant(m_ApiKey));
    QByteArray json = build_json("startSession");
    QString sign = sign_request(json);
    QNetworkRequest request(QUrl(m_BaseUrl+"?sig="+sign));
    //qDebug() << json;
    QObject::connect(m_Network->post(request, json), SIGNAL(finished()), this, SLOT(onInit()));
}
void GrooveHandler::authenticate(QString p_Username, QString p_Password){
    QVariantMap parameters;
    parameters.insert("login", QVariant(p_Username));
    parameters.insert("password", QVariant(QCryptographicHash::hash(p_Password.toUtf8(), QCryptographicHash::Md5).toHex()));
    QByteArray json_data = build_json("authenticate", parameters);
    QString sign = sign_request(json_data);
    QNetworkRequest request(QUrl(m_BaseUrl+"?sig="+sign));
    QObject::connect(m_Network->post(request, json_data), SIGNAL(finished()), this, SLOT(onLogin()));
}
void GrooveHandler::getUserLibrarySongs(int limit, int page){
    QNetworkRequest request;
    QVariantMap parameters;
    if(limit && page){
        parameters.insert("limit", limit);
        parameters.insert("page", page);
    }
    QByteArray json_data = build_json("getUserLibrarySongs", parameters);
    QString sign = sign_request(json_data);
    request.setUrl(QUrl(m_BaseUrl+"?sig="+sign));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
    QObject::connect(m_Network->post(request, json_data), SIGNAL(finished()), this, SLOT(onLibraryReceived()));
}
void GrooveHandler::getPopularSongs(int limit){
    QVariantMap parameters;
    if(limit){
        parameters.insert("limit", limit);
    }
    QByteArray json_data = build_json("getPopularSongsToday", parameters);
    QString sign = sign_request(json_data);
    QNetworkRequest request(QUrl(m_BaseUrl+"?sig="+sign));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
    QObject::connect(m_Network->post(request, json_data), SIGNAL(finished()), this, SLOT(onPopularSongsReceived()));
}
void GrooveHandler::getCountry(){
    QByteArray json_data = build_json("getCountry");
    QString sign = sign_request(json_data);
    QNetworkRequest request(m_BaseUrl+"?sig="+sign);
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
    QObject::connect(m_Network->post(request, json_data), SIGNAL(finished()), this, SLOT(onCountryReceived()));
}
void GrooveHandler::getUserFavoriteSongs(int limit){
    QVariantMap parameters;
    if(limit){
        parameters.insert("limit", limit);
    }
    QByteArray json_data = build_json("getUserFavoriteSongs", parameters);
    QString sign = sign_request(json_data);
    QNetworkRequest request(QUrl(m_BaseUrl+"?sig="+sign));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
    QObject::connect(m_Network->post(request, json_data), SIGNAL(finished()), this, SLOT(onFavoritesReceived()));
}
void GrooveHandler::getStreamKey(int p_SongID, bool p_LowBitrate){
    QVariantMap parameters;
    if(p_LowBitrate){
        parameters.insert("lowBitrate", p_LowBitrate);
    }
    parameters.insert("songID", p_SongID);
    parameters.insert("country", m_Country);
    QByteArray json_data = build_json("getSubscriberStreamKey", parameters);
    QString sign = sign_request(json_data);
    //qDebug() << QString(json_data).remove("\n");
    QNetworkRequest request(QUrl(m_BaseUrl+"?sig="+sign));
    request.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/x-www-form-urlencoded"));
    QObject::connect(m_Network->post(request, json_data), SIGNAL(finished()), this, SLOT(onStreamKeyReceived()));
}

QString GrooveHandler::sign_request(QByteArray p_Message){
    QByteArray innerPadding(64, 0x36);
    QByteArray outerPadding(64, 0x5c);
    QByteArray total, part;
    for(int ii = 0; ii < m_ApiSecret.length(); ii++){
        innerPadding[ii] = innerPadding[ii] ^ m_ApiSecret.toUtf8().at(ii);
        outerPadding[ii] = outerPadding[ii] ^ m_ApiSecret.toUtf8().at(ii);
    }
    total = outerPadding;
    part = innerPadding;
    part.append(p_Message);
    total.append(QCryptographicHash::hash(part, QCryptographicHash::Md5));
    return QCryptographicHash::hash(total, QCryptographicHash::Md5).toHex();
}
QByteArray GrooveHandler::build_json(QString p_Method, QVariantMap p_Parameters){
    QByteArray return_value;
    bb::data::JsonDataAccess jda;
    QVariantMap data;
    data.insert("method", QVariant(p_Method));
    if(!p_Parameters.isEmpty()){
        data.insert("parameters", QVariant(p_Parameters));
    }
    data.insert("header", QVariant(m_Header));
    jda.saveToBuffer(data, &return_value);
    //qDebug() << QString(return_value).remove("\n") << "\n";
    return return_value;
}
bool GrooveHandler::hasError(QNetworkReply *p_Reply){
    if(p_Reply->error() == QNetworkReply::NoError){
        return false;
    }
    else{
        bb::system::SystemToast toast;
        toast.setBody("There was an error: "+p_Reply->errorString());
        toast.exec();
        return true;
    }
}
bb::cascades::GroupDataModel* GrooveHandler::fillDataModel(QVariantList p_Songs){
    QStringList keys;
    QList<QVariantMap> data;
    foreach(const QVariant& item, p_Songs){
        data.append(item.toMap());
    }
    keys.append("SongName");
    bb::cascades::GroupDataModel* dataModel = new bb::cascades::GroupDataModel(data, keys, this);
    dataModel->setGrouping(bb::cascades::ItemGrouping::ByFirstChar);
    return dataModel;
}
void GrooveHandler::onInit(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    bb::data::JsonDataAccess jda;
    if(!hasError(reply)){
        QVariantMap response = jda.loadFromBuffer(reply->readAll()).toMap();
        if(response["result"].toMap()["success"].toBool()){
            m_Header.insert("sessionID", response["result"].toMap()["sessionID"]);
        }
        else{
            qDebug() << "ERROR";
        }
    }
    reply->deleteLater();
}
void GrooveHandler::onLogin(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    bb::data::JsonDataAccess jda;
    QVariantMap response = jda.loadFromBuffer(reply->readAll()).toMap()["result"].toMap();
    if(!hasError(reply)){
        if(response["success"].toBool()){
            bb::system::SystemToast log_toast;
            log_toast.setBody("Welcome "+response["FName"].toString());
            log_toast.exec();
            emit LoggedIn();
            getUserLibrarySongs();
            getPopularSongs();
            getUserFavoriteSongs();
            getCountry();
        }
        else{
            bb::system::SystemToast toast;
            toast.setBody("Wrong username or password!");
            toast.exec();
        }
    }
    reply->deleteLater();
}
void GrooveHandler::onLibraryReceived(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    bb::data::JsonDataAccess jda;
    if(!hasError(reply)){
        QVariantMap response = jda.loadFromBuffer(reply->readAll()).toMap()["result"].toMap();
        emit LibraryRefreshed(fillDataModel(response["songs"].toList()));
    }
    reply->deleteLater();
}
void GrooveHandler::onPopularSongsReceived(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    bb::data::JsonDataAccess jda;
    if(!hasError(reply)){
        QVariantMap response = jda.loadFromBuffer(reply->readAll()).toMap()["result"].toMap();
        emit PopularRefreshed(fillDataModel(response["songs"].toList()));
    }
    reply->deleteLater();
}
void GrooveHandler::onCountryReceived(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    bb::data::JsonDataAccess jda;
    if(!hasError(reply)){
        m_Country = jda.loadFromBuffer(reply->readAll()).toMap()["result"].toMap();
    }
    reply->deleteLater();
}
void GrooveHandler::onFavoritesReceived(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    bb::data::JsonDataAccess jda;
    if(!hasError(reply)){
        QVariantMap response = jda.loadFromBuffer(reply->readAll()).toMap()["result"].toMap();
        emit FavoritesRefreshed(fillDataModel(response["songs"].toList()));
    }
    reply->deleteLater();
}
void GrooveHandler::onTimeout(){
    QTimer* timer = qobject_cast<QTimer*>(sender());
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(timer->parent());
    delete timer;
    reply->abort();
}
void GrooveHandler::onStreamKeyReceived(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    if(!hasError(reply)){
        bb::data::JsonDataAccess jda;
        QVariantMap response = jda.loadFromBuffer(reply->readAll()).toMap()["result"].toMap();
        emit newStreamKey(response["url"].toString());
    }
}

#ifdef QT_DEBUG
void GrooveHandler::debugReply(){
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    qDebug() << QString(reply->readAll()).remove("\n");
    qDebug() << reply->errorString();
    reply->deleteLater();
}
#endif
