#ifndef GROOVEHANDLER_H
#define GROOVEHANDLER_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QVariantMap>
#include <QtNetwork/QNetworkReply>
#include <bb/cascades/GroupDataModel>
class GrooveHandler : public QObject
{
    Q_OBJECT
public:
    explicit GrooveHandler(QObject *parent = 0);
    Q_INVOKABLE void authenticate( QString p_Username, QString p_Password );
    void getUserLibrarySongs(int limit = 0, int page = 0);
    void getPopularSongs(int limit = 0);
    void getUserFavoriteSongs(int limit = 0);
    Q_INVOKABLE void getStreamKey(int p_SongID, bool p_LowBitrate = false);
private:
    QNetworkAccessManager* m_Network;
    const QString m_BaseUrl;
    const QString m_ApiSecret;
    const QString m_ApiKey;
    QVariantMap m_Header;
    QVariantMap m_Country;
    QString sign_request( QByteArray p_Message );
    QByteArray build_json( QString p_Method, QVariantMap p_Parameters = QVariantMap() );
    void getCountry();
    bool hasError(QNetworkReply* p_Reply);
    bb::cascades::GroupDataModel* fillDataModel(QVariantList p_Songs);
    QNetworkReply* post_request(QNetworkRequest p_Request, QByteArray p_Json_Data);
signals:
    void LoggedIn();
    void LibraryRefreshed(bb::cascades::DataModel* p_Library);
    void PopularRefreshed(bb::cascades::DataModel* p_Popular);
    void FavoritesRefreshed(bb::cascades::DataModel* p_Favorites);
    void newStreamKey(QString p_StreamKey);
private slots:
#ifdef QT_DEBUG
    void debugReply();
#endif
    void onInit();
    void onLogin();
    void onLibraryReceived();
    void onPopularSongsReceived();
    void onCountryReceived();
    void onFavoritesReceived();
    void onTimeout();
    void onStreamKeyReceived();

};

#endif // GROOVEHANDLER_H
