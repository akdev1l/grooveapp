TEMPLATE = app

LIBS += -lbbdata -lbb -lbbcascades -lbbsystem -lbbmultimedia
QT += declarative xml

SOURCES += \
    src/main.cpp \
    src/applicationui.cpp \
    src/groovehandler.cpp \
    src/webimageview.cpp \
    src/mediaplayerhandler.cpp

HEADERS += \
    src/applicationui.h \
    src/groovehandler.h \
    src/webimageview.h \
    src/mediaplayerhandler.h

OTHER_FILES += \
    bar-descriptor.xml \
    assets/main.qml \
    assets/LoginPage.qml \
    assets/SongItem.qml \
    assets/PopularPage.qml \
    assets/HeaderItem.qml \
    assets/FavoritesPage.qml



